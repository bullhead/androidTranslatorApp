package com.bullhead.translator.domain;

import java.io.Serializable;

public class Translation implements Serializable {
    private long id;
    private long time;
    private String translation;
    private String text;
    private String sourceLang;
    private String destLang;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSourceLang() {
        return sourceLang;
    }

    public void setSourceLang(String sourceLang) {
        this.sourceLang = sourceLang;
    }

    public String getDestLang() {
        return destLang;
    }

    public void setDestLang(String destLang) {
        this.destLang = destLang;
    }
}
