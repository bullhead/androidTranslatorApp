package com.bullhead.translator.ui.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.Window;

import com.bullhead.translator.R;


public class TranslatorProgressDialog extends Dialog {
    public TranslatorProgressDialog(@NonNull Context context) {
        super(context);
        if (getWindow() != null) {
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        setCancelable(false);
        setContentView(R.layout.dialog_progress);
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            //ignore
        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
            //ignore
        }
    }
}