package com.bullhead.translator.ui.main.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bullhead.translator.R;
import com.bullhead.translator.backend.DbHelper;
import com.bullhead.translator.ui.main.adapters.HistoryAdapter;

public class HistoryFragment extends Fragment{
    private Context context;
    private DbHelper dbHelper;
    private RecyclerView historyListView;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        dbHelper = new DbHelper(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        setupViews(view);
        return view;
    }


    private void setupViews(View view) {
        final SwipeRefreshLayout swipeRefreshLayout = view.findViewById(R.id.swipeLayout);
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        historyListView = view.findViewById(R.id.historyListView);
        historyListView.setLayoutManager(new LinearLayoutManager(context));
        historyListView.setAdapter(new HistoryAdapter(dbHelper.getTranslations(), context));
        swipeRefreshLayout.setRefreshing(false);
    }

    private void loadData() {
        historyListView.setAdapter(new HistoryAdapter(dbHelper.getTranslations(), context));
    }
}
