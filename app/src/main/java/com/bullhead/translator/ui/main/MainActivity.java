package com.bullhead.translator.ui.main;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.bullhead.translator.R;
import com.bullhead.translator.ui.main.adapters.PagerViewAdapter;
import com.bullhead.translator.ui.main.fragments.AboutUsFragment;
import com.bullhead.translator.ui.main.fragments.HistoryFragment;
import com.bullhead.translator.ui.main.fragments.TranslateFragment;

public class MainActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        android.support.v7.widget.Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tabLayout=findViewById(R.id.tabLayout);
        viewPager=findViewById(R.id.viewPage);
        setupViewPager();
    }

    private void setupViewPager() {
        String titles[] = {getString(R.string.translate), getString(R.string.previous_translations), getString(R.string.about_us)};
        Fragment[] fragments = {new TranslateFragment(), new HistoryFragment(), new AboutUsFragment()};
        viewPager.setAdapter(new PagerViewAdapter(getSupportFragmentManager(),titles,fragments));
        tabLayout.setupWithViewPager(viewPager);
        setTabLayoutIcons();
    }

    private void setTabLayoutIcons() {
        TabLayout.Tab tab1=tabLayout.getTabAt(0);
        if (tab1!=null){
            tab1.setIcon(R.drawable.ic_translate);
        }
        TabLayout.Tab tab2=tabLayout.getTabAt(1);
        if (tab2!=null){
            tab2.setIcon(R.drawable.ic_history_black_24dp);
        }
        TabLayout.Tab tab3 = tabLayout.getTabAt(2);
        if (tab3 != null) {
            tab3.setIcon(R.drawable.ic_info_outline_black_24dp);
        }
    }
}
