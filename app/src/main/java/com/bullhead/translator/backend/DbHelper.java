package com.bullhead.translator.backend;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;

import com.bullhead.translator.domain.Translation;

import java.util.ArrayList;
import java.util.List;

public class DbHelper extends SQLiteOpenHelper {
    private static final String CREATE_TABLE_TRANSLATION = "CREATE TABLE " + TranslationEntry.TABLE_NAME + " ( " +
            TranslationEntry._ID + " INTEGER PRIMARY KEY , " +
            TranslationEntry.COLUMN_TRANSLATION + " TEXT , " +
            TranslationEntry.COLUMN_TEXT + " TEXT , " +
            TranslationEntry.COLUMN_DESTINATION + " TEXT , " +
            TranslationEntry.COLUMN_SOURCE + " TEXT , " +
            TranslationEntry.COLUMN_TIME + " INTEGER )";
    private static final String[] PROJECTION = {TranslationEntry._ID,
            TranslationEntry.COLUMN_TRANSLATION,
            TranslationEntry.COLUMN_SOURCE,
            TranslationEntry.COLUMN_DESTINATION,
            TranslationEntry.COLUMN_TEXT,
            TranslationEntry.COLUMN_TIME
    };

    public DbHelper(Context context) {
        super(context, "translation_db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_TRANSLATION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @NonNull
    public List<Translation> getTranslations() {
        SQLiteDatabase db = getWritableDatabase();
        List<Translation> translations = new ArrayList<>();
        Cursor cursor = db.query(TranslationEntry.TABLE_NAME, PROJECTION, null,
                null, null, null, null);
        while (cursor.moveToNext()) {
            translations.add(cursorToTranslation(cursor));
        }
        cursor.close();
        return translations;
    }

    public void saveTranslation(@NonNull Translation translation) {
        ContentValues values = new ContentValues();
        values.put(TranslationEntry.COLUMN_DESTINATION, translation.getDestLang());
        values.put(TranslationEntry.COLUMN_SOURCE, translation.getSourceLang());
        values.put(TranslationEntry.COLUMN_TEXT, translation.getText());
        values.put(TranslationEntry.COLUMN_TIME, translation.getTime());
        values.put(TranslationEntry.COLUMN_TRANSLATION, translation.getTranslation());
        getWritableDatabase().insert(TranslationEntry.TABLE_NAME, null, values);
    }

    private Translation cursorToTranslation(Cursor cursor) {
        Translation translation = new Translation();
        translation.setId(cursor.getLong(0));
        translation.setTranslation(cursor.getString(1));
        translation.setSourceLang(cursor.getString(2));
        translation.setDestLang(cursor.getString(3));
        translation.setText(cursor.getString(4));
        translation.setTime(cursor.getLong(5));
        return translation;
    }

    private static class TranslationEntry implements BaseColumns {
        private static final String TABLE_NAME = "translation_entry";
        private static final String COLUMN_TRANSLATION = "translation";
        private static final String COLUMN_TEXT = "text";
        private static final String COLUMN_SOURCE = "source";
        private static final String COLUMN_DESTINATION = "destination";
        private static final String COLUMN_TIME = "time";
    }
}
